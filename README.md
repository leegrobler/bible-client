# MiBible

> Another Vue.js creation, MiBible uses both a Node.js back-end and Firebase (Google's Back-end-as-a-Service). The inspiration behind this project was to be able to provide a service to the community, by making traditionally written text available for all online.
>  
> The platform provides access to scripture online and asks for donations, not for profit but to assist charities that assist people with mental and physical disabilities. An associate was responsible for the web design component, while I was responsible for implementing his designs in code.

[Check it out](https://gospel-of-christ.firebaseapp.com/)

## Run Locally

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

#### Author

Lee Grobler  
hello@lee-grobler.com  

### Technologies Used

 - HTML
 - CSS
 - Javascript
 - Vue
 - Vuetify
 - Firebase

import firebase from 'firebase/app';
import 'firebase/firestore';

// firebase init goes here
const config = { // i haven't moved any of this to an env file, coz we currently only store public info.
	apiKey: "AIzaSyDmfjqyONG6DM2Lf4cmDqAYps0ntWdlxJ0",
	authDomain: "gospel-of-christ.firebaseapp.com",
	databaseURL: "https://gospel-of-christ.firebaseio.com",
	projectId: "gospel-of-christ",
	storageBucket: "gospel-of-christ.appspot.com",
	messagingSenderId: "349124663138"
};
firebase.initializeApp(config);

// firebase utils
const db = firebase.firestore();

// firebase collections
const verseCollection = db.collection('verses');
const donateCollection = db.collection('donations');
const bibleCollection = db.collection('bible');

// // this is temporary
// const bibles = db.collection(`bibles`);
// const kjv = db.collection(`bibles/yUsj04LdSXcBGfDU74h4/languages/mpPcoIKBSM8oMCTPJ2U9/books`);
// const chapters = book => db.collection(`bibles/yUsj04LdSXcBGfDU74h4/languages/mpPcoIKBSM8oMCTPJ2U9/books/${book}/chapters`);


export { db, verseCollection, donateCollection, bibleCollection };
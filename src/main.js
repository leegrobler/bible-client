import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';
import store from './store';

import Snotify from 'vue-snotify';
import 'vue-snotify/styles/material.css';
import VueResource from 'vue-resource';
import VueAnalytics from 'vue-analytics';

Vue.config.productionTip = false;

Vue.use(VueResource);

Vue.use(Snotify, {
  toast: {
    timeout:10000,
    showProgressBar:false,
    bodyMaxLength:200
  }
});

Vue.use(VueAnalytics, {
  id:'UA-111973446-3',
  router,
  autoTracking: {
    exception: true,
  },
  debug: {
    // enabled: process.env.NODE_ENV !== 'production',
    enabled: false,
    sendHitTask: process.env.NODE_ENV === 'production'
  }
});

window.moment = require('moment');

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');

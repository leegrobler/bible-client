import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';
import Scroll from 'vuetify/es5/directives/scroll';

Vue.use(Vuetify, {
  iconfont: 'md',
  directives: {
    Scroll
  }
});

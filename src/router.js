import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Book from './views/Book.vue'
import Books from './views/Books.vue'

import Vuex from './store';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path:'/', name:'Home', component:Home },
    { path:'/books', name:'Books', component:Books },
    { path:'/books/:book', name:'Book', component:Book, beforeEnter:(to, from, next) => {
      if(!from.name) next('/'); // if you land on a book page, show index while loading book

      Vuex.dispatch('setBook', to.params.book).then(ret => {
        if(!from.name) next(to.path); // if you land on a book page, show index while loading book
        else next();
      }).catch(err => {
        Vuex.dispatch('displayNotification', err);
      });
    }},
  ]
});

import Vue from 'vue';
import Vuex from 'vuex';

const fb = require('./firebase');

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		books: undefined,
		book: undefined,
		verse: undefined,
		parallaxHeight: 0,
		snackbar: {
			show: false,
			text: '',
			timeout: 3000,
		},
		donation: 0.00,
	},
	getters: {
		books: state => {
			return state.books;
		},
		book: state => {
			return state.book;
		},
		verse: state => {
			return state.verse;
		},
		parallaxHeight: state => {
			return state.parallaxHeight;
		},
		snackbar: state => {
			return state.snackbar;
		},
		donation: state => {
			return state.donation;
		},
	},
	mutations: {
		setBooks: (state, payload) => {
			state.books = payload;
		},
		setBook: (state, payload) => {
			state.book = payload;
		},
		setVerse: (state, payload) => {
			state.verse = payload;
		},
		setParallaxHeight: (state, payload) => {
			state.parallaxHeight = payload;
		},
		setSnackbar: (state, payload) => {
			state.snackbar = payload;
		},
		setDonation: (state, payload) => {
			state.donation = payload;
		},
	},
  actions: {
    setBooks: ({ getters, commit }) => {
			return new Promise((resolve, reject) => {
				if(!getters.books) {
					fb.bibleCollection.doc('Index').get().then(res => {
						commit('setBooks', res.data());
						return resolve();
					}).catch(() => reject({ title: 'Error', msg: 'Unable to retrieve Books. Please try again later. [F1]', type: 'failed' }));
				} else return resolve();
			});
		},
		setBook: ({ getters, commit, dispatch }, payload) => {
			return new Promise((resolve, reject) => {
				if(!getters.book || getters.book.book.replace(/ /g, '') !== payload) {
					dispatch('displaySnack', { text:`Retrieving ${payload}. Please wait.`, timeout:3000, show:true });
					commit('setBook', undefined);

					fb.bibleCollection.doc(payload).get().then(res => {
						commit('setBook', res.data());
						return resolve();
					}).catch(() => reject({ title: 'Error', msg: `Unable to retrieve ${payload}. Please try again later. [F1]`, type: 'failed' }));
				} else return resolve();
			});
		},
		setVerse({ commit }) {
			fb.verseCollection.where('date', '==', new moment().format('DD/MM')).get().then(snapshot => {
				if(snapshot.docs.length > 0) {
					const book = snapshot.docs[0].data().title.split(' ');
					const chapt = book[1].split(':')[0];
					const verse = book[1].split(':')[1].split('-').map(v => parseInt(v) - 1);

					fb.bibleCollection.doc(book[0]).get().then(res => {
						commit('setVerse', {
							title: snapshot.docs[0].data().title,
							body: res.data().chapters[chapt - 1].verses.map((v, i) => v[i + 1])
							.filter((v, i) => i >= verse[0] && i <= verse[verse.length - 1]).join(' ')
						});
					});
				}
			}).catch(err => {});
		},
		setDonation({ commit }, payload) {
			fb.donateCollection.orderBy('date', 'desc').limit(1).get().then(snapshot => {
				if(snapshot.docs.length > 0) commit('setDonation', snapshot.docs[0].data().amount);
			}).catch(err => {});
		},
		displayNotification({}, payload) {
			// console.log('payload:', payload);
			let options = {};
			if(payload.sticky) {
				if(typeof payload.sticky === 'object') options = payload.sticky;
				else options = { 
					timeout: 3000000,
					buttons: [{ text: 'Thanks', action: undefined, bold: true }],
					backdrop: 0.5,
					bodyMaxLength: 500
				}
			}

			if(!payload.title) {
				if(payload.type === 'failed') payload.title = 'Error';
				else payload.title = payload.type.substring(0, 1).toUpperCase()+payload.type.substring(1,payload.type.length);
			}

			const type = payload.type === 'danger' || payload.type === 'failed' ? 'error' : payload.type;
			Vue.prototype.$snotify[type](payload.msg, payload.title, options);
		},
		displaySnack: ({ commit }, payload) => commit('setSnackbar', payload),
		setParallaxHeight: ({ commit }, payload) => commit('setParallaxHeight', payload),
  }
});
